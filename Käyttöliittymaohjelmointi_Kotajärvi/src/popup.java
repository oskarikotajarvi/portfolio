import javax.swing.JOptionPane;

public class popup extends JOptionPane {
	
	private static final long serialVersionUID = 1L;

	public popup(String infotext, String title) {
		
		JOptionPane.showMessageDialog(null, infotext, title, JOptionPane.INFORMATION_MESSAGE);
	}

}
