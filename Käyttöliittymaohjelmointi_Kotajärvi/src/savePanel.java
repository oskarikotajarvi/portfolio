import java.awt.Dimension;
import java.awt.GridBagConstraints;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class savePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public savePanel() {
		
		Dimension size = getPreferredSize();
		size.width = 600;
		size.height = 50;
		setPreferredSize(size);
		
		String[] kk = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
		String[] vv = {"2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"};
		
		JLabel tyoLabel = new JLabel("Ty�paikka: ");
		JTextField tyoField = new JTextField(10);
		JLabel kkLabel = new JLabel("Kuukausi: ");
		JComboBox<String> kkBox = new JComboBox<String>(kk);
		JLabel vuosi = new JLabel("Vuosi: ");
		JComboBox<String> vvBox = new JComboBox<String>(vv);
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.gridx = 0;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;
		add(tyoLabel, gc);
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		add(tyoField, gc);
		
		gc.gridx = 0;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		add(kkLabel, gc);
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		add(kkBox, gc);
		
		gc.gridx = 0;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.LINE_START;
		add(vuosi, gc);
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		add(vvBox, gc);
		
	}

}

