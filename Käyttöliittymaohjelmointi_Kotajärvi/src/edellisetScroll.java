import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

public class edellisetScroll extends JScrollPane {
	
	private static final long serialVersionUID = 1L;

	public edellisetScroll() {
		
		Dimension size = getPreferredSize();
		size.width = 600;
		size.height = 400;
		setPreferredSize(size);
		
		setBorder(BorderFactory.createTitledBorder(null, "Tallennetut palkat", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.ITALIC, 25), Color.BLACK));

	}

}
