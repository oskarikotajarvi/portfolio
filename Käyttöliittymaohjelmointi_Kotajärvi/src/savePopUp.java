import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JOptionPane;

public class savePopUp extends JOptionPane {
	
	private static final long serialVersionUID = 1L;
	
	private savePanel savePanel;
	
	public savePopUp() {
		
		savePanel = new savePanel();
		
		Object options[] = {"Tallenna", "Peruuta"};
		
		int n = JOptionPane.showOptionDialog(null, savePanel, "Palkan tallennus", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null,  options, options[1]);
		
		savePanel.addPropertyChangeListener(new PropertyChangeListener() {

			public void propertyChange(PropertyChangeEvent e) {
				if(n == JOptionPane.YES_OPTION) {
					String infotext = "Palkka tallennettu";
					String title = "Palkan tallennus";
					popup popup = new popup(infotext, title);
				}
				
			}
			
		});
	}

}

