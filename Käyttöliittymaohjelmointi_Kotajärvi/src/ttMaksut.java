import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class ttMaksut extends JPanel {
	
	private static final long serialVersionUID = 1L;
	

	public ttMaksut() {
		
		setVisible(false);
		
		Dimension size = getPreferredSize();
		size.width = 1000;
		size.height = 400;
		setPreferredSize(size);
		
		Font btnFont= new Font("Arial", Font.BOLD, 10);
		
		setBorder(BorderFactory.createTitledBorder(null, "Palkasta pidätetään", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.ITALIC, 25), Color.BLACK));
		
		//Ennakkopidätyskentät
		JLabel ennakkoLabel = new JLabel("Ennakkopidätysprosentti: ");
		JTextField ennakkoField = new JTextField(10);
		JTextField ennakkoOutput = new JTextField(10);
		JButton ennakkoBtn = new JButton("?");
		ennakkoBtn.setBorderPainted(false);
		ennakkoBtn.setBackground(null);
		ennakkoBtn.setFont(btnFont);
		ennakkoField.setEditable(true);
		ennakkoOutput.setEditable(false);
		ennakkoBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Prosentin saat työntekijän verkortista. Ennakkopidätysprosentti sisältää valiton veron,\nkunnallisveron ja kirkonveron.";
				String title = "Ennakkopidätysprosentti";
				popup popup = new popup(infotext, title);
			}
		});
		
		//TYEL
		JLabel tyelLabel = new JLabel("Työeläkemaksu (TYEL): ");
		JTextField tyelField = new JTextField("6,15%", 10);
		JTextField tyelOutput = new JTextField(10);
		JButton tyelBtn = new JButton("?");
		tyelBtn.setFont(btnFont);
		tyelField.setEditable(false);
		tyelOutput.setEditable(false);
		tyelBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Työeläkemaksu maksetaan kaiksita 17-67-vuotiaista työntekijöistä. 53-ikävuoden jälkeen prosentti on korkeampi.\nPalkkalaskuri ei huomioi kuukausittaista alarajatuloa."
						+ " Varsinaisessa palkanlaskennassa se huomioidaan automaattisesti.";
				String title = "Työeläkemaksu (TYEL)";
				popup popup = new popup(infotext, title);
			}
		});
		
		//tvmMAksu
		JLabel tvmMaksuLabel = new JLabel("Työttömyysvakuutusmaksu: ");
		JTextField tvmMaksuField = new JTextField("1,6%", 10);
		JTextField tvmMaksuOutput = new JTextField(10);
		JButton tvmMaksuBtn = new JButton("?");
		tvmMaksuBtn.setFont(btnFont);
		tvmMaksuField.setEditable(false);
		tvmMaksuOutput.setEditable(false);
		tvmMaksuBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Työntekijän palkasta pidätetään työttömyysvakuutusmaksu, kun työntekijä "
						+ "on täyttänyt\n17-vuotta ja on alle 65-vuotias. Työttömyysvakuusmaksu on 1.6% ja se pidätetään\n"
						+ "vaikka työntantajan maksama palkkasumma olisi enintään 1200€.";
				String title = "Työttyömyysvakuutusmaksu";
				popup popup = new popup(infotext, title);
			}
		});
		
		//Ennakkopidätykset yhteensä
		JLabel maksutLabel = new JLabel("Ennakkopidätykset yhteensä: ");
		JTextField maksutField = new JTextField(10);
		maksutLabel.setFont(new Font("Arial", Font.BOLD, 20));
		maksutField.setEditable(false);
		
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		//////////////////PLACEMENTS//////////////////////
		gc.weightx = 0.01;
		gc.weighty = 0.01;
		
		//Ennakkopidätys placement
		gc.gridx = 0;
		gc.gridy = 0;
		add(ennakkoBtn, gc);
		gc.gridx = 1;
		add(ennakkoLabel, gc);
		gc.gridx = 2;
		add(ennakkoField, gc);
		gc.gridx = 3;
		add(ennakkoOutput, gc);
		
		//TYEL placement
		gc.gridx = 0;
		gc.gridy = 1;
		add(tyelBtn, gc);
		gc.gridx = 1;
		add(tyelLabel, gc);
		gc.gridx = 2;
		add(tyelField, gc);
		gc.gridx = 3;
		add(tyelOutput, gc);
		
		//tvmMaksut palcement
		gc.gridx = 0;
		gc.gridy = 2;
		add(tvmMaksuBtn, gc);
		gc.gridx = 1;
		add(tvmMaksuLabel, gc);
		gc.gridx = 2;
		add(tvmMaksuField, gc);
		gc.gridx = 3;
		add(tvmMaksuOutput, gc);
		
		//Ennakkopidätys yhteensä placement
		gc.gridx = 2;
		gc.gridy = 3;
		add(maksutLabel, gc);
		gc.gridx = 3;
		add(maksutField, gc);
		
	}

}
