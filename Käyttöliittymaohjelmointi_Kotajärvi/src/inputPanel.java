import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class inputPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public inputPanel() {
		
		setVisible(false);
		
		Dimension size = getPreferredSize();
		size.width = 1000;
		size.height = 50;
		setPreferredSize(size);
		
		//setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		Font btnFont= new Font("Arial", Font.BOLD, 10);
		
		JButton palkkaBtn = new JButton("?");
		palkkaBtn.setFont(btnFont);
		JButton ikaBtn = new JButton("?");
		ikaBtn.setFont(btnFont);
		JLabel palkkaLabel = new JLabel("Palkka: ");
		JLabel ikaLabel = new JLabel("Ik�: ");
		TextField palkkaInput = new TextField(10);
		TextField ikaInput = new TextField(10);
		
		palkkaBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Sy�t� bruttopalkka";
				String title = "Palkka";
				popup popup = new popup(infotext, title);
			}
		});
		
		ikaBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Ik� vaikuttaa prosenttien suuruuteen";
				String title = "Ik�";
				popup popup = new popup(infotext, title);
			}
		});
		
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();

		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.gridx = 0;
		gc.gridy = 0;
		add(palkkaBtn, gc);
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		add(palkkaLabel, gc);
		gc.gridx = 2;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		add(palkkaInput, gc);
		
		gc.gridx = 0;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		add(ikaBtn, gc);
		gc.gridx = 1;
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		add(ikaLabel,gc);
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.gridx = 2;
		add(ikaInput, gc);
		
	}
}
