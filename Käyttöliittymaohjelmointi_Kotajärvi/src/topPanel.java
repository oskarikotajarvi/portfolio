import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class topPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public topPanel() {
		
		//SetSize
		Dimension size = getPreferredSize();
		size.width = 1000;
		size.height = 50;
		setPreferredSize(size);
		
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		JLabel palkkaLabel = new JLabel("Palkka");
		palkkaLabel.setFont(new Font("Arial", Font.BOLD, 50));
		
		
		final JButton sulje = new JButton("Sulje Palkka");
		sulje.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		/*Dimension size2 = sulje.getPreferredSize();
		size2.width = 100;
		size2.height = 100;
		sulje.setPreferredSize(size2);*/
		
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.weighty = 0.5;
		gc.weightx = 0.5;
		
		//Palkkalabel
		gc.anchor = GridBagConstraints.LINE_START;
		add(palkkaLabel, gc);
		
		//Sulkunappi
		gc.anchor = GridBagConstraints.LINE_END;
		gc.fill = GridBagConstraints.VERTICAL;
		gc.ipadx = 100;
		add(sulje, gc);
		
		gc.fill = 0;
		
		
	}
}

