import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Main {

	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				
				JFrame frame = new MainFrame("Palkka");
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1050, 900);
				frame.setMinimumSize(new Dimension(800, 600));
				//frame.setResizable(false); this was for testing purposes
			}
		});	
	}

}
