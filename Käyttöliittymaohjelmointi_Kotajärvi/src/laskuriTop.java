import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class laskuriTop extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public laskuriTop() {
		
		setVisible(false);
				
		Dimension size = getPreferredSize();
		size.width = 1000;
		size.height = 50;
		setPreferredSize(size);
		
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		JLabel palkkalaskuriLabel = new JLabel("Palkkalaskuri");
		palkkalaskuriLabel.setFont(new Font("Arial", Font.BOLD, 50));
		
		JButton info = new JButton("Info");
		info.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String infotext = "Palkkalaskurin avulla voit arvioida palkan sivukuluja, sek� ty�ntekij�lle maksettavaa palkkaa.\n" + "Laskennassa k�ytet��n keskiarvoisia prosentteja.";
				String title = "Info";
				popup popup = new popup(infotext, title);
			}
			
		});
		
		/*Dimension size2 = info.getPreferredSize();
		size2.width = 100;
		size2.height = 100;
		info.setPreferredSize(size2);*/  // t�m� pois
		
		
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.weighty = 0.5;
		gc.weightx = 0.5;
		
		//Palkkalabel
		gc.anchor = GridBagConstraints.LINE_START;
		add(palkkalaskuriLabel, gc);
		
		//Sulkunappi
		gc.anchor = GridBagConstraints.LINE_END;
		gc.fill = GridBagConstraints.VERTICAL;
		gc.ipadx = 100;
		add(info, gc);
		
		gc.fill = 0;
		
		
	}

}
