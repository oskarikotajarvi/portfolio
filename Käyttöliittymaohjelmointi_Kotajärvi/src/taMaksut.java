import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class taMaksut extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public taMaksut() {
		
		setVisible(false);
		
		Dimension size = getPreferredSize();
		size.width = 1000;
		size.height = 400;
		setPreferredSize(size);
		
		Font btnFont = new Font("Arial", Font.BOLD, 10);
		
		setBorder(BorderFactory.createTitledBorder(null, "Työnantajan maksut", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.ITALIC, 25), Color.BLACK));
		
		//Työeläkemaksukentät
		JLabel temLabel = new JLabel("Työeläkemaksu: ");
		JTextField temField = new JTextField("18,95%", 10);
		temField.setEditable(false);
		JTextField temOutput = new JTextField(10);
		temOutput.setEditable(false);
		JButton temBtn = new JButton("?");
		temBtn.setFont(btnFont);
		temBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Työeläkemaksu koostuu työnantajan ja työntekijän osuudesta. Tässä kentässä on työnantajan osuus.";
				String title = "Työeläkemaksu";
				popup popup = new popup(infotext, title);
			}
		});

		
		//Sairausvakuutusmaksukentät
		JLabel svmLabel = new JLabel("Sairausvakuutusmaksu: ");
		JTextField svmField = new JTextField("1,08%", 10);
		svmField.setEditable(false);
		JTextField svmOutput = new JTextField(10);
		svmOutput.setEditable(false);
		JButton svmBtn = new JButton("?");
		svmBtn.setFont(btnFont);
		svmBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Sairausvakuutusmaksu maksetaan verohallinnolle yhdessä muiden oma-aloitteisten verojen kanssa.\n"
						+ "Kaikilla tämä on sama prosentti, mutta sairausvakuutusmaksua ei tarvitse maksaa jos työntekijä on\nalle 16- tai yli 68-vuotias.";
				String title = "Sairausvakuutusmaksu";
				popup popup = new popup(infotext, title);
			}
		});
		
		//Muut pakolliset
		JLabel muutLabel = new JLabel("Muut pakolliset vakuutukset: ");
		JTextField muutField = new JTextField(10);
		muutField.setEditable(true);
		JTextField muutOutput = new JTextField(10);
		muutOutput.setEditable(false);
		JButton muutBtn = new JButton("?");
		muutBtn.setFont(btnFont);
		muutBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Työnantajan täytyy ottaa työntekijälle työtapaturma- ja ammattivakuutus. Vakuutusmaksu vaihtelee\naloittain. Tarkista"
						+ " maksun suuruus vahinkovakuutusyhtiöltä tai palkkahallintoltasi.";
				String title = "Muut pakolliset maksut";
				popup popup = new popup(infotext, title);
			}
		});
		
		//Työttömyysvakuusmaksu
		JLabel tvmLabel = new JLabel("Työttömyysvakuutusmaksu: ");
		JTextField tvmField = new JTextField(10);
		tvmField.setEditable(false);
		JTextField tvmOutput = new JTextField(10);
		tvmOutput.setEditable(false);
		JButton tvmBtn = new JButton("?");
		tvmBtn.setFont(btnFont);
		tvmBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Työnantajan työttömyysvakuutusmaksuvelvollisuus koskee työntekijöitä, joilla on palkanmaksua yli 1200€\n"
						+ "kalenterivuoden aikana. Maksu on palkasta 0,8% 2059500 euroon asti, jonka ylittältä osalta maksu on 3,3%";
				String title = "Työttömyysvakuutusmaksu";
				popup popup = new popup(infotext, title);
			}
		});
		
		//Muut kulut
		JLabel muutKulutLabel = new JLabel("Muut kulut: ");
		JTextField muutKulutField = new JTextField(10);
		muutKulutField.setEditable(true);
		JTextField muutKulutOutput = new JTextField(10);
		muutKulutOutput.setEditable(false);
		JButton muutKulutBtn = new JButton("?");
		muutKulutBtn.setFont(btnFont);
		muutKulutBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String infotext = "Muita kuluja ovat esimerkiksi lomakorvaus, puhelinkorvaus, työkalukorvaus, työterveys jne.";
				String title = "Muut kulut";
				popup popup = new popup(infotext, title);
			}
		});
		
		//Työntantajan maksettavat
		JLabel maksettava = new JLabel("Työnantajan maksut yhteensä: ");
		JTextField maksettavaOutput = new JTextField(10);
		maksettavaOutput.setEditable(false);
		maksettava.setFont(new Font("Arial", Font.BOLD, 20));
		
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		//////////////////PLACEMENTS//////////////////////
		gc.weightx = 0.01;
		gc.weighty = 0.01;
		
		//Työeläkemaksu placement
		gc.gridx = 0;
		gc.gridy = 0;
		add(temBtn, gc);
		gc.gridx = 1;
		add(temLabel, gc);
		gc.gridx = 2;
		add(temField, gc);
		gc.gridx = 3;
		add(temOutput, gc);
		
		//Sairausvakuutusmaksu placement
		gc.gridx = 0;
		gc.gridy = 1;
		add(svmBtn, gc);
		gc.gridx = 1;
		add(svmLabel, gc);
		gc.gridx = 2;
		add(svmField, gc);
		gc.gridx = 3;
		add(svmOutput, gc);
		
		//Muut pakolliset vakuutukset placement
		gc.gridx = 0;
		gc.gridy = 2;
		add(muutBtn, gc);
		gc.gridx = 1;
		add(muutLabel, gc);
		gc.gridx = 2;
		add(muutField, gc);
		gc.gridx = 3;
		add(muutOutput, gc);
		
		//Työttömyysvakuusmaksu
		gc.gridx = 0;
		gc.gridy = 3;
		add(tvmBtn, gc);
		gc.gridx = 1;
		add(tvmLabel, gc);
		gc.gridx = 2;
		add(tvmField, gc);
		gc.gridx = 3;
		add(tvmOutput, gc);
		
		//Muut kulut placement
		gc.gridx = 0;
		gc.gridy = 4;
		add(muutKulutBtn, gc);
		gc.gridx = 1;
		add(muutKulutLabel, gc);
		gc.gridx = 2;
		add(muutKulutField, gc);
		gc.gridx = 3;
		add(muutKulutOutput, gc);
		
		//Työnantajan maksettava
		gc.gridx = 2;
		gc.gridy = 5;
		add(maksettava, gc);
		gc.gridx = 3;
		add(maksettavaOutput, gc);
		
	}

}
