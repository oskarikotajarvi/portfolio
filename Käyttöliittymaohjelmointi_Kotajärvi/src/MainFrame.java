import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MainFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private topPanel topPanel;
	private edellisetScroll edellisetScroll;
	private laskuriTop laskuriTop;
	private taMaksut taMaksut;
	private inputPanel inputPanel;
	private ttMaksut ttMaksut;

		
	public MainFrame(String title) {
		
		super(title);
				
		//Set Layout Manager
		setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		Font btnFont = new Font("Arial", Font.BOLD, 20);
		
		//Create Swing Components
		JLabel maksettavaLabel = new JLabel("Maksettava palkka: ");
		JButton laskuri = new JButton("Palkkalaskuri");
		JButton tallenna = new JButton("Tallenna palkkalaskelma");
		JButton poistu = new JButton("Poistu laskurista");
		
		topPanel = new topPanel();
		edellisetScroll = new edellisetScroll();
		laskuriTop = new laskuriTop();
		taMaksut = new taMaksut();
		inputPanel = new inputPanel();
		ttMaksut = new ttMaksut();
		
		
		//Add the default visibilities and button fonts etc
		maksettavaLabel.setFont(new Font("Arial", Font.BOLD, 40));
		maksettavaLabel.setVisible(false);
		tallenna.setVisible(false);
		tallenna.setSize(new Dimension(100,50));
		tallenna.setFont(btnFont);
		poistu.setVisible(false);
		poistu.setSize(new Dimension(100,100));
		poistu.setFont(btnFont);
		laskuri.setPreferredSize(new Dimension(200,100));
		laskuri.setFont(btnFont);
		
		//Add the button actionlisteners
		laskuri.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				topPanel.setVisible(false);
				edellisetScroll.setVisible(false);
				laskuri.setVisible(false);
				laskuriTop.setVisible(true);
				taMaksut.setVisible(true);
				inputPanel.setVisible(true);
				ttMaksut.setVisible(true);
				maksettavaLabel.setVisible(true);
				tallenna.setVisible(true);
				poistu.setVisible(true);
			}
			
		});
		
		tallenna.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				savePopUp savePopUp = new savePopUp();
			}
			
		});
		
		poistu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				topPanel.setVisible(true);
				edellisetScroll.setVisible(true);
				laskuri.setVisible(true);
				laskuriTop.setVisible(false);
				taMaksut.setVisible(false);
				inputPanel.setVisible(false);
				ttMaksut.setVisible(false);
				maksettavaLabel.setVisible(false);
				tallenna.setVisible(false);
				poistu.setVisible(false);
			}
			
		});
		
		//Add Swing Components to content pane
		Container c = getContentPane();
		
		gc.weightx = 0.5;
		gc.weighty = 0.5;
		
		////////////////////////////////MainScreenComponents///////////////////////////////////////////////
		gc.gridx = 0;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.NORTH;
		gc.fill = GridBagConstraints.HORIZONTAL;
		c.add(topPanel, gc);
		
		gc.fill = 0;
		
		gc.anchor = GridBagConstraints.CENTER;
		gc.gridx = 0;
		gc.gridy = 1;
		//gc.fill = GridBagConstraints.VERTICAL; //not a good decision
		c.add(laskuri, gc);
		
		gc.fill = 0;
		
		gc.anchor = GridBagConstraints.CENTER;
		gc.gridx = 0;
		gc.gridy = 2;
		gc.fill = GridBagConstraints.HORIZONTAL;
		c.add(edellisetScroll, gc);
		
		gc.fill = 0;
		
		////////////////////////////////PalkkalaskuriComponents///////////////////////////////////////////////
		gc.weighty = 0.1;
		
		//Laskuri topPanel
		gc.gridx = 0;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.NORTH;
		gc.fill = GridBagConstraints.HORIZONTAL;
		c.add(laskuriTop, gc);
		
		gc.fill = 0;
		
		//Laskurin inputPanel
		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.gridy = 1;
		gc.fill = GridBagConstraints.HORIZONTAL;
		c.add(inputPanel, gc);
		
		//Työnantajan maksut
		//gc.gridx = 0;
		gc.gridy = 2;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		c.add(taMaksut, gc);
		
		//Työntekijän maksut
		gc.gridy = 3;
		gc.fill = GridBagConstraints.HORIZONTAL;
		gc.anchor = GridBagConstraints.LINE_START;
		c.add(ttMaksut, gc);
		
		//Maksettavaa yhteensä
		gc.fill = 0;
		gc.gridy = 4;
		gc.anchor = GridBagConstraints.LINE_START;
		c.add(maksettavaLabel, gc);
		
		//Poistu / tallenna napit
		gc.gridy = 5;
		gc.anchor = GridBagConstraints.LINE_START;
		c.add(tallenna, gc);
		gc.anchor = GridBagConstraints.LINE_END;
		c.add(poistu, gc);
		}
	}
